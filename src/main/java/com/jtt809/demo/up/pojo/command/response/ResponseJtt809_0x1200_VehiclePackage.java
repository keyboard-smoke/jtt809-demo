package com.jtt809.demo.up.pojo.command.response;

import com.jtt809.demo.up.pojo.BasePackage;
import com.jtt809.demo.up.pojo.Response;
import io.netty.buffer.ByteBuf;
import lombok.Data;

/**
 * 车辆信息基本类
 */
@Data
public abstract class ResponseJtt809_0x1200_VehiclePackage extends Response {

    /**
     * 车牌号
     */
    private String vehicleNo;

    /**
     * 车辆颜色，按照 JT/T415-2006中 5.4.12 的规定
     * 1 - 蓝色
     * 2 - 黄色
     * 3 - 黑色
     * 4 - 白色
     * 9 - 其他
     */
    private byte vehicleColor;

    /**
     * 子业务类型标识
     */
    private int dataType;

    /**
     * 后续数据长度
     */
    protected int dataLength;

    /**
     * 解码车辆基本数据
     *
     * @param buf
     */
    @Override
    protected void decodeImpl(ByteBuf buf) {
        // 车牌
        this.vehicleNo = buf.readBytes(21).toString(BasePackage.DEFAULT_CHARSET_GBK).trim();
        // 车牌颜色
        this.vehicleColor = buf.readByte();
        // 子业务类型
        this.dataType = buf.readUnsignedShort();
        // 后续数据长度
        this.dataLength = buf.readInt();

        // 解码具体车辆数据实现方法
        decodeDataImpl(buf);
    }

    /**
     * 解码具体车辆数据实现方法
     *
     * @param buf
     */
    protected abstract void decodeDataImpl(ByteBuf buf);

}
