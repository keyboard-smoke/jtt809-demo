package com.jtt809.demo.up.listener;

import cn.hutool.core.thread.ThreadUtil;
import com.jtt809.demo.down.PrimaryLinkClientManager;
import com.jtt809.demo.down.SlaveLinkClientManager;
import com.jtt809.demo.up.business.BusinessFactory;
import com.jtt809.demo.up.config.Jtt809Config;
import com.jtt809.demo.up.manager.PrimaryLinkManagerJtt809;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * <p>监听应用启动</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/15 14:50
 */
@Component
public class ApplicationReadyListener implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        //启动jtt809上级平台主链路
        if (Jtt809Config.JTT809_NETTY_SERVER_UP_OPEN) {
            startupUpPrimaryLink();
        }

        //启动jtt809下级平台主链路
        if (Jtt809Config.JTT809_NETTY_SERVER_DOWN_OPEN) {
            startupDownPrimaryLink();
        }
    }

    //启动jtt809上级平台主链路
    public void startupUpPrimaryLink() {
        //启动主链路
        ThreadUtil.execute(new PrimaryLinkManagerJtt809(Jtt809Config.JTT809_NETTY_SERVER_UP_IP, Jtt809Config.JTT809_NETTY_SERVER_UP_PORT));
        // 延迟5s
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 队列处理数据
        ThreadUtil.execute(new BusinessFactory());
    }

    //启动jtt809下级平台主链路
    public static void startupDownPrimaryLink() {
        // 启动下级平台客户端
        ThreadUtil.execute(new PrimaryLinkClientManager(Jtt809Config.JTT809_NETTY_SERVER_UP_IP,
                Jtt809Config.JTT809_NETTY_SERVER_UP_PORT,
                Jtt809Config.JTT809_NETTY_SERVER_DOWN_IP,
                Jtt809Config.JTT809_NETTY_SERVER_DOWN_PORT));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 启动下级平台服务端
        ThreadUtil.execute(new SlaveLinkClientManager(Jtt809Config.JTT809_NETTY_SERVER_DOWN_IP,
                Jtt809Config.JTT809_NETTY_SERVER_DOWN_PORT));
    }
}
