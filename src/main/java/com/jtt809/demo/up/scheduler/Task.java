package com.jtt809.demo.up.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * <p>定时任务</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/15 13:44
 */
@Component
public class Task {

    @Resource
    private TaskProcess taskProcess;

    /**
     * <p>批量更新车辆最新定位信息
     *    每隔n毫秒执行一次</p>
     */
    @Scheduled(fixedRate = 20 * 1000)
    public void updateLocations() {
        taskProcess.updateLocations();
    }

    /**
     * <p>GPS定位时间超时未刷新则更改为离线</p>
     */
//    @Scheduled(fixedRate = 2 * 60 * 1000)
    public void updateOffline() {

    }

}
