package com.jtt809.demo.up.business.impl;

import com.jtt809.demo.up.business.IBusinessServer;
import com.jtt809.demo.up.pojo.command.response.ResponseJtt809_0x9002;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

/**
 * 从链路连接应答信息
 * 链路类型:从链路。
 * 消息方问:下级平台往上级平台。
 * 业务数据类型标识:DOWN_CONNNECT_RSP。
 * 描述：下级平台作为服务器端向上级平台客户端返回从链路连接应答消息，上级平台在接收到该应答消息结果后，根据结果进行链路连接处理
 */
@Slf4j
public class ResponseHandlerImpl_0x9002 implements IBusinessServer<ResponseJtt809_0x9002> {

    public void businessHandler(ChannelHandlerContext ctx, ResponseJtt809_0x9002 msg) throws InterruptedException {

        switch (msg.getResult()) {
            case 0:
                msg.setIsLoginFlagFromDownPlatform(true);
                log.info("=====> 【上级平台|信息】下级平台登录成功！");
                break;
            case 1:
                msg.setIsLoginFlagFromDownPlatform(false);
                log.info("=====> 【上级平台|信息】校验码错误！");
                break;
            case 2:
                msg.setIsLoginFlagFromDownPlatform(false);
                log.info("=====> 【上级平台|信息】资源紧张，稍后再连接（ 已经占用)！");
                break;
            default:
                msg.setIsLoginFlagFromDownPlatform(false);
                log.info("=====> 【上级平台|信息】其它");
        }

    }

}
