package com.jtt809.demo.up.util;

import com.jtt809.demo.up.pojo.TrackCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>缓存工具类</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/15 11:20
 */
@Slf4j
public class CacheUtil {

    /**
     * 车牌号关联车辆ID
     */
    public static Map<String, Long> NO_RELATE_ID_MAP = new ConcurrentHashMap<>();

    public static void putNoRelateId(String no, Long id) {
        if (StringUtils.isEmpty(no)) return;
        NO_RELATE_ID_MAP.put(no, id);
    }

    public static long getNoRelateId(String no) {
        return Optional.ofNullable(NO_RELATE_ID_MAP.get(no)).orElse(0l);
    }

    /**
     * 车辆ID关联定位信息
     */
    synchronized
    public static void putIdRelateLocation(Long id, TrackCache trackCache) {
        if (trackCache.getGpsTime() == null) return;
//        TrackCache preTrackCache = (TrackCache) RedisUtil.RedisMap.get(RedisUtil.TYPE_ID_RELATE_LOCATION_MAP, id);
//        if (preTrackCache != null
//                && preTrackCache.getGpsTime().after(trackCache.getGpsTime())) {
//            return;//已缓存的定位时间在新入定位时候晚，则丢弃此次缓存
//        }

        //一分钟内的定位信息则更新车辆状态为--->在线
        if (System.currentTimeMillis() - trackCache.getGpsTime().getTime() <= 60 * 1000) {
            trackCache.setOnlineStatus(TrackCache.ONLINE_STATUS_ONLINE);
        }
        log.info("缓存信息{}", trackCache);
        //RedisUtil.RedisMap.put(RedisUtil.TYPE_ID_RELATE_LOCATION_MAP, id, trackCache);
    }
}
