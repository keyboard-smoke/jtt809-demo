package com.jtt809.demo.up.util;

import com.jtt809.demo.up.config.Jtt809Config;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>redis工具类</p>
 *
 * @author Allen Yang
 * @datetime 2021/9/14 13:54
 */
//@Component
public class RedisUtil {


    public static final String TYPE_UP_EXG_MSG_REAL_LOCATION_COUNT = "UP_EXG_MSG_REAL_LOCATION_COUNT_SOURCE" + Jtt809Config.JTT809_SOURCE;
    public static final String TYPE_UP_EXG_MSG_REAL_LOCATION_STARTTIME = "UP_EXG_MSG_REAL_LOCATION_STARTTIME_SOURCE" + Jtt809Config.JTT809_SOURCE;

    public static final String TYPE_UP_EXG_MSG_HISTORY_LOCATION_COUNT = "UP_EXG_MSG_HISTORY_LOCATION_COUNT_SOURCE" + Jtt809Config.JTT809_SOURCE;
    public static final String TYPE_UP_EXG_MSG_HISTORY_LOCATION_STARTTIME = "UP_EXG_MSG_HISTORY_LOCATION_STARTTIME_SOURCE" + Jtt809Config.JTT809_SOURCE;

    public final static String TYPE_ID_RELATE_LOCATION_MAP = "ID_RELATE_LOCATION_MAP_SOURCE" + Jtt809Config.JTT809_SOURCE;

//    @Resource
//    private RedisTemplate<Object, Object> redisTemplate;
//
//    public static RedisTemplate<Object, Object> REDIS_TEMPLATE;
//
//    @PostConstruct
//    public void postConstruct() {
//        REDIS_TEMPLATE = redisTemplate;
//    }

    public static Map<String, Object> CACHE_MAP = new HashMap<>();

    private static String buildKey(String type, String key) {
        return type + "_" + key;
    }

    public static Object get(String type, String key) {
//        return REDIS_TEMPLATE.opsForValue().get(buildKey(type, key));
        return CACHE_MAP.get(buildKey(type, key));
    }

    public static void set(String type, String key, Object value) {
//        REDIS_TEMPLATE.opsForValue().set(buildKey(type, key), value);
        CACHE_MAP.put(buildKey(type, key), value);
    }

    public static class RedisMap {
        public static void put(String type, Object hk, Object hv) {
//            REDIS_TEMPLATE.opsForHash().put(type, hk, hv);
            CACHE_MAP.put(type + hk, hv);
        }

        public static Object get(String type, Object hk) {
//            return REDIS_TEMPLATE.opsForHash().get(type, hk);
            return CACHE_MAP.get(type + hk);
        }

        public static Map entries(String type) {
//            return REDIS_TEMPLATE.opsForHash().entries(type);
            return CACHE_MAP;
        }

        public static void delete(String type, Object... hks) {
            //REDIS_TEMPLATE.opsForHash().delete(type, hks);
            Arrays.stream(hks).forEach(hk -> {
                CACHE_MAP.remove(type + hk);
            });
        }
    }

}
